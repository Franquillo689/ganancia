import numpy as np

valores_comerciales = [10, 12, 15, 18, 
											22, 27, 33, 39, 
											47, 56, 68, 82]

genes = [1 , 1.2, 1.5, 1.8, 
				2.2, 2.7, 3.3, 3.9, 
				4.7, 5.6, 6.8, 8.2]

for i in valores_comerciales:
	for j in range(0,6):
		genes.append(i*10**j)

genes = np.array(genes)


v1 = 0
v2 = 0.002

def funcion(r1,r2,r3,r4):
	return (1+2*(r3/r4))*(r2/r1)


class DNA:
	def __init__(self, n_individuals, target, mutation_rate):
		self.n_individuals = n_individuals
		self.target        = target
		self.mutation_rate = mutation_rate
		self.generation    = 1
		self.ind_size      = 4
		self.fitness       = np.array([])
		self.population    = []
		self.verbose       = False

	def create_population(self):
		self.population = [ [] for i in range(self.n_individuals) ]
		for i in range(self.n_individuals):
			for _ in range(self.ind_size):
				self.population[i].append(genes[np.random.randint(0,len(genes))])

	def calculate_fitness(self):
		fitness_list = []
		for i in self.population:
			fitness_list.append(funcion(i[0],i[1],i[2],i[3]))
		fitness_list = np.array(fitness_list)
		fitness_list = fitness_list / self.target
		self.fitness = fitness_list
		self.generation += 1

	def best_individual(self):
		# Return best individual
		fitness   = self.fitness[0]
		index = 0
		for i in range(self.n_individuals):
			if abs(1 - self.fitness[i]) < fitness:
				index = i
				fitness   = abs(1 - self.fitness[i])

		best_ind = self.population[index]
		gan = funcion(best_ind[0],best_ind[1],best_ind[2],best_ind[3])

		for i in best_ind:
			if i < 1000:
				return

		if gan > self.target-5  and gan < self.target+5:
			print("##################################")
			print("R1 = ", best_ind[0], " ohm")
			print("R2 = ", best_ind[1], " ohm")
			print("R3 = ", best_ind[2], " ohm")
			print("R4 = ", best_ind[3], " ohm")
			print("Ganancia: ",gan)
			print("##################################")
"""
	def crossover(self):
		# Do the reproduction of
		# best individuals.
		offspring = []
		for i in range(self.n_individuals//2):
			parents      = np.random.choice(self.n_individuals, 2, p = self.fitness)
			cross_point  = np.random.randint(self.ind_size)

			#print([self.population[parents[0]][:cross_point] + self.population[parents[1]][cross_point:]])
			#print([self.population[parents[1]][:cross_point] + self.population[parents[0]][cross_point:]])

			offspring   += [self.population[parents[0]][:cross_point] + self.population[parents[1]][cross_point:]]
			offspring   += [self.population[parents[1]][:cross_point] + self.population[parents[0]][cross_point:]]
		self.population    = offspring
		self.n_individuals = len(self.population)
		self.generation   += 1

	def mutation(self):
		# Mutate new population
		for i in range(self.n_individuals):
			ind_dna = self.population[i]
			for j in range(self.ind_size):
				prob = np.random.random()
				mutation = genes[np.random.randint(0,len(genes))]
				if prob < self.mutation_rate:
					ind_dna[j] = mutation
			self.population[i] = ind_dna
"""





popmax = 4000
target = int(input("Ingrese la ganancia: ")) # Ganancia
mutation_rate = 0.1
model = DNA(
				n_individuals = popmax,
				target        = target,
				mutation_rate = mutation_rate
			  )



while True:
	model.create_population()
	model.calculate_fitness()
	model.best_individual()
